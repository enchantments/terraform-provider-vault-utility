module gitlab.com/enchantments/terraform-provider-vaultutility

go 1.14

require (
	github.com/cenkalti/backoff/v4 v4.0.2
	github.com/hashicorp/terraform-plugin-sdk v1.12.0
	github.com/hashicorp/vault/api v1.0.4
)
